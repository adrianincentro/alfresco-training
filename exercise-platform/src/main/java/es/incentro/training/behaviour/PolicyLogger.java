package es.incentro.training.behaviour;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.alfresco.model.ContentModel;

import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.repo.node.NodeServicePolicies.*;


import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import es.incentro.training.models.Models;
import es.incentro.training.service.BehaviourService;

import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.repo.policy.*;
import org.alfresco.repo.policy.Behaviour.NotificationFrequency;



public class PolicyLogger
implements NodeServicePolicies.OnCreateNodePolicy
{
	@Autowired
	private BehaviourService behaviourService;
	
	private Logger logger = Logger.getLogger(PolicyLogger.class);
	// Behaviours
	private Behaviour onCreateNode;
	// Dependencies
	private PolicyComponent policyComponent;
	//Este metodo será el que se ejecutará cuandos se cargue la clase
	public void init() {
		if (logger.isDebugEnabled())
		logger.debug("Initializing policy logger behaviour");
		
		this.onCreateNode = new JavaBehaviour(this,OnCreateNodePolicy.QNAME.getLocalName(),NotificationFrequency.TRANSACTION_COMMIT);
		

	    	 this.policyComponent.bindClassBehaviour(OnCreateNodePolicy.QNAME,this.logger,this.onCreateNode);
		}
	@Override
	public void onCreateNode(ChildAssociationRef arg0) {
		behaviourService.MoveExcel(arg0);
	}
	//Debemos crear un setPolicy para que esta se pueda cambiar
		public void setPolicyComponent(PolicyComponent policyComponent) 
		{
			this.policyComponent = policyComponent;
		}
	
}
