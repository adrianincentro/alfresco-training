package es.incentro.training.cronjob;

import java.util.List;

import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import es.incentro.training.rules.rules2;
import es.incentro.training.service.CronJobService;


public class CustomAction2 extends ActionExecuterAbstractBase {
	
	@Autowired
	   private CronJobService cronJobService;


	public static final String NAME = "cron borrar";
	private static final Logger logger = LoggerFactory.getLogger(rules2.class);
 public static final String PARAM_ASPECT_NAME = "aspect-name";
 
 /**
 * the node service
 */
 private NodeService nodeService;

 /**
 * Set the node service
 * 
 * @param nodeService
 * the node service
 */
 public void setNodeService(NodeService nodeService) {
 this.nodeService = nodeService;
 }
 
 
 

 @Override
 protected void executeImpl(Action action, NodeRef actionedUponNodeRef) {
	 logger.debug("cronjob2");
	 cronJobService.mostrar("PATH: 'app:company_home/cm:HTML_x0020_files//*'","HTML files",nodeService);
	 cronJobService.mostrar("PATH: 'app:company_home/cm:Rejected_x0020_Documents//*'","Rejected Documents",nodeService);
	 cronJobService.mostrar("PATH: 'app:company_home/cm:Excel_x0020_files//*'","Excel files",nodeService);
	 
	 

 }


 @Override
 protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
  paramList.add(new ParameterDefinitionImpl(PARAM_ASPECT_NAME,
  DataTypeDefinition.QNAME, true,
  getParamDisplayLabel(PARAM_ASPECT_NAME)));

 }

}