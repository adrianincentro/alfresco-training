package es.incentro.training.rules;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.MimetypeService;
import org.alfresco.service.cmr.repository.ContentService;
import es.incentro.training.models.Models;
import es.incentro.training.service.RulesService;

public class rules extends ActionExecuterAbstractBase {
   public static final String NAME = "rules";

   @Autowired
   private RulesService rulesService;

   @Override
   protected void executeImpl(Action action, NodeRef node) {
	   rulesService.ConvertHtmlExcel(action, node);

   }

   @Override
   protected void addParameterDefinitions(List<ParameterDefinition> arg0) {

   }
}

