package es.incentro.training.models;

import org.alfresco.api.AlfrescoPublicApi;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;

import es.incentro.training.rules.namespace0;


/**
 * Content Model Constants
 */
@AlfrescoPublicApi
public interface Models
{
	static final QName HTMLTYPE_CONTENT = QName.createQName(namespace0.ypa_URI, "htmlType");
	static final QName EXCELTYPE_CONTENT = QName.createQName(namespace0.ypa_URI, "excelType");
	
	static final QName LOCATION_CONTENT = QName.createQName(namespace0.ypa_URI, "location");
	static final QName PLOCATION_CONTENT = QName.createQName(namespace0.ypa_URI, "previousLocation");
}