package es.incentro.training.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.UserTransaction;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.MimetypeService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import es.incentro.training.models.Models;
import es.incentro.training.rules.rules2;

public class RulesService {
	
	@Autowired
	   private NodeService nodeService;
	   @Autowired
	   private MimetypeService mimetypeService;
	   @Autowired
	   private ContentService ContentService;
	   @Autowired
	   private SearchService SearchService;
	   @Autowired
		private TransactionService transactionService;
	   
	   private static final Logger logger = LoggerFactory.getLogger(RulesService.class);
	   
	   public void MoveHtml(Action action,NodeRef node) {
		   final String fileName = (String) nodeService.getProperty(node, ContentModel.PROP_NAME);
		   final String mimetype = mimetypeService.guessMimetype(fileName);
		   logger.debug("Nodem="+node);
	      if (mimetype.equals("text/html")) {
	         nodeService.setType(node, Models.HTMLTYPE_CONTENT);
	         SearchParameters sp = new SearchParameters();
	         sp.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
	         sp.setQuery("PATH: 'app:company_home/cm:HTML_x0020_files'");
	         sp.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
	         ResultSet rs = SearchService.query(sp);
	         if (rs.length()>0) {
	        	 List<NodeRef> n0 = rs.getNodeRefs();
	        	 NodeRef n = n0.get(0);
	         logger.debug("nn="+n);
	         NodeRef node2 = n; //new NodeRef("workspace://SpacesStore/446c38b2-5ed7-4880-878a-d7741bf2889f");
	         Map<QName, Serializable> props = new HashMap<QName, Serializable>(2);
	         UserTransaction tx = transactionService.getUserTransaction();
	         try {
				 tx.begin();
	         props.put(Models.PLOCATION_CONTENT,  node.toString());
	         nodeService.addAspect(node,Models.LOCATION_CONTENT, props);
	         nodeService.moveNode(node,node2,null,null);
	         logger.debug("Node1store="+node.getStoreRef().toString());
	         logger.debug("Node2is="+node2);
	         tx.commit();
	         }catch(Exception e) {
	        	 logger.debug(e.getMessage());
	         }finally {
					try {
						tx.rollback();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.debug(e.getMessage());
					}
					}
	         }else{
	        	 logger.debug("NODE IS 0"); 
	         }
	      }
	   }
	   
	   public void ConvertHtmlExcel(Action action,NodeRef node) {
		   final String fileName = (String) nodeService.getProperty(node, ContentModel.PROP_NAME);
		   final String mimetype = mimetypeService.guessMimetype(fileName);
		   logger.debug("Node="+mimetype);
	      if (mimetype.equals("text/html")) {
	         nodeService.setType(node, Models.HTMLTYPE_CONTENT);
	         logger.debug("Node2Html="+mimetypeService.guessMimetype(fileName));
	      }else if(mimetype.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) { {
	    	  nodeService.setType(node, Models.EXCELTYPE_CONTENT);
	    	  logger.debug("Node2Excel="+mimetypeService.guessMimetype(fileName));
	      }
	      }
	   }

}
