package es.incentro.training.service;

import java.util.List;

import javax.transaction.UserTransaction;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.MimetypeService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.transaction.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import es.incentro.training.rules.rules2;

public class CronJobService {
	
	@Autowired
	 private SearchService SearchService;
	@Autowired
	 private MimetypeService mimetypeService;
	@Autowired
	private TransactionService transactionService;
	
	private static final Logger logger = LoggerFactory.getLogger(rules2.class);
	
	public void DeleteOthers(Action action, NodeRef actionedUponNodeRef,NodeService nodeService) {
		logger.debug("CronJob Eliminar Archivos");
		 
		 SearchParameters sp = new SearchParameters();
	     sp.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
	     sp.setQuery("PATH: 'app:company_home/cm:Rejected_x0020_Documents'");
	     sp.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
	     ResultSet rs = SearchService.query(sp);
	     if (rs.length()>0) {
	    	 List<NodeRef> n0 = rs.getNodeRefs();
	    	 NodeRef nodep = n0.get(0);
	    	 
	    	 SearchParameters sp2 = new SearchParameters();
	         sp2.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
	         sp2.setQuery("PATH: 'app:company_home/cm:Rejected_x0020_Documents//*'");
	         sp2.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
	         ResultSet rs2 = SearchService.query(sp2);
	         if (rs2.length()>0) {
	        	 List<NodeRef> nodes = rs2.getNodeRefs();
	        	 int count = 0;
	        	 for(NodeRef n: nodes) {
	        		 final String fileName = (String) nodeService.getProperty(n, ContentModel.PROP_NAME);
	        		 final String mimetype = mimetypeService.guessMimetype(fileName);
	        		 if (!mimetype.equals("text/html") && !mimetype.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
	        			 count++;
	        			 UserTransaction tx = transactionService.getUserTransaction();
	        			 try {
	        				 tx.begin();
	        			 nodeService.removeChild(nodep, n);
	        			 logger.debug("Eliminado: "+fileName);
	        			 tx.commit();
	        			 }catch(Exception e){
	        				 logger.debug(e.getMessage());
	        			 }finally {
	        					try {
	        						tx.rollback();
	        					} catch (Exception e) {
	        						// TODO Auto-generated catch block
	        						logger.debug(e.getMessage());
	        					}
	        					}
	        		 }
	        		 }
	        	 logger.debug("Archivos eliminados: "+count);
	         }
	    	 }else {
	    		 logger.debug(" La carpeta Rejected Documents no existe");
	    	 }
	}
	
	public void mostrar(String path,String nombre,NodeService nodeService) {
		 SearchParameters sp = new SearchParameters();
	     sp.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
	     sp.setQuery(path);
	     sp.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
	     ResultSet rs = SearchService.query(sp);
	     if (rs.length()>0) {
	    	 List<NodeRef> nodes = rs.getNodeRefs();
	    	 int count = 0;
	    	 logger.info("---- Informacion de "+nombre+" ----");
	    	 for(NodeRef n: nodes) {
	    		 count++;
	    		 final String fileName = (String) nodeService.getProperty(n, ContentModel.PROP_NAME);
	    		 final String mimetype = mimetypeService.guessMimetype(fileName);
	    		 logger.info("Nombre: "+fileName+" Tipo: "+mimetype);
	    	 }
	    	 logger.info("Elementos: "+count);
	     } 
	 }

}
